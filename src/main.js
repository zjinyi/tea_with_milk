import Vue from 'vue';
import AMap from 'vue-amap';
import App from './App';
import router from './router';
import Axios from 'axios';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';    // 默认主题
// import '../static/css/theme-green/index.css';       // 浅绿色主题
import '../static/css/icon.css';
import "babel-polyfill";
import qs from 'qs';
import  VueQuillEditor from 'vue-quill-editor'
// require styles 引入样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Axios.defaults.withCredentials = true

var gretUrl = '' ;
if (process.env.NODE_ENV === 'development') {

  // development本地测试环境
  // npm run dev
  
  gretUrl=Axios.defaults.baseURL = 'https://qkj.zjinyi.cn/'
  // gretUrl=Axios.defaults.baseURL = 'https://tea-hlbx-dongyi.bidou88.cn/'
    // gretUrl=Axios.defaults.baseURL = 'https://tea-taipu-tianhe.bidou88.cn/'
} else {
  // npm run build
  // gretUrl=Axios.defaults.baseURL = 'https://mtadmin.zjinyi.cn/'

// gretUrl=Axios.defaults.baseURL = 'https://tea-tai-feel-daxue.bidou88.cn/'
// gretUrl=Axios.defaults.baseURL = 'https://tea-xqc-nancun.bidou88.cn/'
// gretUrl=Axios.defaults.baseURL = 'https://tea-taipu-tianhe.bidou88.cn/'
// gretUrl=Axios.defaults.baseURL = 'https://tea-taning-fanhualu.bidou88.cn/'
// gretUrl = Axios.defaults.baseURL = 'https://tea-hlbx-dongyi.bidou88.cn/'
  // gretUrl=Axios.defaults.baseURL = 'https://tea-heitaoman-zhongcun.bidou88.cn/'
  gretUrl=Axios.defaults.baseURL = 'https://malatang-tangxia-heguanglu.bidou88.cn/'
  /*测试*/
  // gretUrl=Axios.defaults.baseURL = 'https://xiajiao.zjinyi.cn/'
  // gretUrl=Axios.defaults.baseURL = 'https://dashi.zjinyi.cn/'
}
// 跨域请求post会提交option,同时数据要变成json字符串
var myInterceptor = Axios.interceptors.request.use((config) => {
  config.data = qs.stringify(config.data)
  return config
}, function (error) {
  return Promise.reject(error)
})

Vue.use(AMap);
Vue.use(ElementUI, { size: 'small' });
Vue.use(VueQuillEditor)
Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
// Axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';

Vue.prototype.$axios = Axios;
Vue.prototype.$qs = qs;
Vue.prototype.$myInterceptor = myInterceptor;
Vue.prototype.$gretUrl = gretUrl; 

AMap.initAMapApiLoader({
    key:'8e75245ecc3501674582c52cdda837b0',
    plugin: ['AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType','Geocoder','Geolocation'],
    uiVersion: '1.0' ,// ui库版本，不配置不加载,
    v: '1.4.4'
});

//使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
    const role = localStorage.getItem('ms_username');
    if(!role && to.path !== '/login'){
        next('/login');
    }else if(to.meta.permission){
        // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
        role === 'admin' ? next() : next('/403');
    }else{
        // 简单的判断IE10及以下不进入富文本编辑器，该组件不兼容
        if(navigator.userAgent.indexOf('MSIE') > -1 && to.path === '/editor'){
            Vue.prototype.$alert('vue-quill-editor组件不兼容IE10及以下浏览器，请使用更高版本的浏览器查看', '浏览器不兼容通知', {
                confirmButtonText: '确定'
            });
        }else{
            next();
        }
    }
})

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');