import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
    {
        path: '/',
        redirect: '/dashboard'
    },
    {
        path: '/',
        component: resolve => require(['../components/common/Home.vue'], resolve),
        meta: { title: '自述文件' },
        children:[
        {
            path: '/dashboard',
            component: resolve => require(['../components/page/Dashboard.vue'], resolve),
            meta: { title: '系统首页' }
        },
        {
            path: '/icon',
            component: resolve => require(['../components/page/Icon.vue'], resolve),
            meta: { title: '自定义图标' }
        },
        {
            path: '/table',
            component: resolve => require(['../components/page/BaseTable.vue'], resolve),
            meta: { title: '基础表格' }
        },
        {
            path: '/tabs',
            component: resolve => require(['../components/page/Tabs.vue'], resolve),
            meta: { title: 'tab选项卡' }
        },
        {
            path: '/form',
            component: resolve => require(['../components/page/BaseForm.vue'], resolve),
            meta: { title: '基本表单' }
        },
        {
            // 富文本编辑器组件
            path: '/editor',
            component: resolve => require(['../components/page/VueEditor.vue'], resolve),
            meta: { title: '富文本编辑器' }
        },
        {
            // markdown组件
            path: '/markdown',
            component: resolve => require(['../components/page/Markdown.vue'], resolve),
            meta: { title: 'markdown编辑器' }    
        },
        {
            // 图片上传组件
            path: '/upload',
            component: resolve => require(['../components/page/Upload.vue'], resolve),
            meta: { title: '文件上传' }   
        },
        {
            // vue-schart组件
            path: '/charts',
            component: resolve => require(['../components/page/BaseCharts.vue'], resolve),
            meta: { title: 'schart图表' }
        },
        {
            // 拖拽列表组件
            path: '/drag',
            component: resolve => require(['../components/page/DragList.vue'], resolve),
            meta: { title: '拖拽列表' }
        },
        {
            // 权限页面
            path: '/permission',
            component: resolve => require(['../components/page/Permission.vue'], resolve),
            meta: { title: '权限测试', permission: true }
        },
        {
            path: '/404',
            component: resolve => require(['../components/page/404.vue'], resolve),
            meta: { title: '404' }
        },
        {
            path: '/403',
            component: resolve => require(['../components/page/403.vue'], resolve),
            meta: { title: '403' }
        },
        
        {
            path: '/user',
            component: resolve => require(['../components/page/user.vue'], resolve),
            meta: { title: '用户管理' }

        },
        {
            path: '/address',
            component: resolve => require(['../components/page/address.vue'], resolve),
            meta: { title: '用户收货地址管理' }

        },
        {
            path: '/type',
            component: resolve => require(['../components/page/type.vue'], resolve),
            meta: { title: '分类管理' }

        },
        {
            path: '/test',
            component: resolve => require(['../components/page/test.vue'], resolve),
            meta: { title: '分类管理' }

        },
        {
            path: '/goods',
            component: resolve => require(['../components/page/goods.vue'], resolve),
            meta: { title: '商品管理' }

        },
        {
            path: '/order',
            component: resolve => require(['../components/page/order.vue'], resolve),
            meta: { title: '点单列表' }

        },
        {
            path: '/shoporder',
            component: resolve => require(['../components/page/shoporder.vue'], resolve),
            meta: { title: '到店列表' }

        },
        {
            path: '/takeaway',
            component: resolve => require(['../components/page/takeaway.vue'], resolve),
            meta: { title: '外卖列表' }

        },
        {
            path: '/allorder',
            component: resolve => require(['../components/page/allorder.vue'], resolve),
            meta: { title: '所有点餐' }

        },
        {
            path: '/activity',
            component: resolve => require(['../components/page/activity.vue'], resolve),
            meta: { title: '分销活动' }

        },
        {
            path: '/coupon',
            component: resolve => require(['../components/page/coupon.vue'], resolve),
            meta: { title: '优惠券' }

        },
        {
            path: '/map',
            component: resolve => require(['../components/page/map.vue'], resolve),
            meta: { title: '地图' }

        },
        {
            path: '/spa',
            component: resolve => require(['../components/page/spa.vue'], resolve),
            meta: { title: 'SPA管理' }
        },
        {
            path: '/strike',
            component: resolve => require(['../components/page/strike.vue'], resolve),
            meta: { title: '发现管理' }
        },
        {
            path: '/space',
            component: resolve => require(['../components/page/space.vue'], resolve),
            meta: { title: '空间管理' }
        },
        {
            path: '/diy',
            component: resolve => require(['../components/page/diy.vue'], resolve),
            meta: { title: 'DIY管理' }
        },
        {
            path: '/set',
            component: resolve => require(['../components/page/set.vue'], resolve),
            meta: { title: '设置规格管理' }
        },
        {
            path: '/parcel',
            component: resolve => require(['../components/page/parcel.vue'], resolve),
            meta: { title: '外卖设置' }
        },
        {
            // path: '/',
            // component: resolve => require(['../components/page/vip.vue'], resolve),
            // children:[{
                path: "/vip",
                component: resolve => require(['../components/page/vip.vue'], resolve),
                meta: { title: '会员卡设置' }

            // },
            // {
            //     path:'/vipuser',
            //     component: resolve => require(['../components/page/vuser.vue'], resolve),
            //     meta: { title: '会员用户' }
            // }]    
        },
        {
                path: "/vipuser",
                component: resolve => require(['../components/page/vipuser.vue'], resolve),
                meta: { title: '会员用户' }
        },
        {
            // path: '/',
            // component: resolve => require(['../components/page/sundry/banner.vue'], resolve),
            // children: [
            // {
                path: "/active-banner",
                component: resolve => require(['../components/page/sundry/banner.vue'], resolve),
                meta: { title: '店铺活动图片管理' }
            // }
            // ]
        }
        //  {
        //     path: '/',
        //     component: resolve => require(['../components/page/set/spec.vue'], resolve),
        //     children:[
        //         {
        //           path:'/spec',
        //           component: resolve => require(['../components/page/set/spec.vue'], resolve),
        //           meta: { title: '规格管理' }
        //         },
        //         {
        //           path:'/feed',
        //           component: resolve => require(['../components/page/set/feed.vue'], resolve),
        //           meta: { title: '加料管理' }
        //         },
        //     ]
        // }
        ]
        },
        {
            path: '/login',
            component: resolve => require(['../components/page/Login.vue'], resolve)
        },
        {
            path: '*',
            redirect: '/404'
        }
        ]
})
